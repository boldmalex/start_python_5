import telebot

constHelp = """
/help - вывести список всех доступных команд
/add - добавить задачу в список дел, пример: /add 25.08.2022 Покормить кошку
/show - вывести список задач на дату, пример /show 25.08.2022
/show_all - вывести список всех задач, пример /show_all
"""

constPrivet = "Привет"

constErrorCommandText = "Неверно задана команда!"
tasks = {}

# Проверка на Привет


def checkPrivet(string):
    if constPrivet in string:
        return True
    else:
        return False

# Добавление задачи в список дел


def addTaskToDict(task, date, dict):
    if date in dict:
        dict[date].append(task)
    else:
        dict[date] = []
        dict[date].append(task)
    return

# Обработчик команды add


def processCommandAdd(messageText):
    answer = ""
    splitted_command = messageText.split(maxsplit=2)

    if len(splitted_command) != 3:
        answer = constErrorCommandText
    else:
        date = splitted_command[1].lower()
        task = splitted_command[2]
        addTaskToDict(task, date, tasks)
        answer = f"Задача '{task}' добавлена на дату {date}"

    return answer


# Обработчик команды show
def processCommandShow(messageText):
    answer = ""
    splitted_command = messageText.split(maxsplit=1)

    if len(splitted_command) != 2:
        answer = constErrorCommandText
    else:
        date = splitted_command[1].lower()
        if date in tasks:
            for task in tasks[date]:
                answer += f"- {task} \n"
        else:
            answer = "На такую дату нет задач"

    return answer


# Обработчик команды show_all
def processCommandShowAll(messageText):
    answer = ""
    splitted_command = messageText.split(maxsplit=1)

    if len(splitted_command) != 1:
        answer = constErrorCommandText
    else:
        if (len(tasks) > 0):
            for date in tasks:
                answer += f"Дата: {date} \n"
                for task in tasks[date]:
                    answer += f"- {task} \n"
        else:
            answer = "Список задач пустой."

    return answer


token = "5742384566:AAGpWIbC17CpS5UCUFptMFkZ-V5gfXqJBPI"

bot = telebot.TeleBot(token)


@bot.message_handler(commands=["help"])
def handleHelp(message):
    bot.send_message(message.chat.id, constHelp)


@bot.message_handler(commands=["add"])
def handleAdd(message):
    answer = processCommandAdd(message.text)
    bot.send_message(message.chat.id, answer)


@bot.message_handler(commands=["show"])
def handleAdd(message):
    answer = processCommandShow(message.text)
    bot.send_message(message.chat.id, answer)


@bot.message_handler(commands=["show_all"])
def handleAdd(message):
    answer = processCommandShowAll(message.text)
    bot.send_message(message.chat.id, answer)


@bot.message_handler(content_types=["text"])
def echo(message):
    answer = ""

    if checkPrivet(message.text):
        answer = "Привет, мопед"
    else:
        answer = "Неизвестная команда. Используйте команду /help для просмотра доступных действий"

    bot.send_message(message.chat.id, answer)


bot.polling(none_stop=True)
